# Brand spanking new Anki Pack
Hi, do you want to build a new Anki pack? You can start with this repository!

If you want to learn, not create new content, see the user guide in
[English](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/GUIDE.md)
or
[German](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/ANLEITUNG.md).

## First-time setup for a new project
Create a
[new project](https://gitlab.phaidra.org/projects/new?namespace_id=2286#import_project)
in the _Anki Packs_ namespace and choose _Repository by URL_ with this repo:
```
https://gitlab.phaidra.org/kartenaale/templates/anki-pack.git
```

This step will also clone this README. You can change it later to remove this
section and tell your users something about your pack instead.

Next, enable Gitlab CI for your project. With this feature, your new content
will produce Anki packages on Merge Requests and on the main branch. To enable,
open _Settings_ in the new Gitlab project and expand
_Visibility, project features, permissions_. _CI/CD_ under _Repository_ should
be _enabled_. Customize Merge Requests and other settings to your liking (
_Pipelines must succeed_ is highly recommended).

*Everything below this point is not starter-project-specific, you may want to keep it*

## Importing the pack to your phone or PC
Open _Deployments | Releases_ on the left, then click _Browse all APKGS…_ on
the newest release. There, click an APKG, download it, and import into Anki.

If you are completely new to Anki and feel a bit lost, check out the user guide
in
[English](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/GUIDE.md)
or
[German](https://gitlab.phaidra.org/kartenaale/getting-started-with-anki/-/blob/main/ANLEITUNG.md).

## Contributing
To add new content to this pack, either add `.apkg` files in the root next to
`content` to include them unchanged, OR add subfolders to `content/` with data
in the form of CSV, images or APKGs and an `.apkg-spec.yaml` file that
configures which HTML to use for the cards and how your data is laid out.

You can use the content in the starter template as your basis or just have a
look to see how it works:
* [content/Facts-from-CSV](https://gitlab.phaidra.org/kartenaale/templates/anki-pack/-/tree/main/content/Facts-from-CSV) for Q/A cards imported from a CSV file,
* [content/Bijection-from-CSV](https://gitlab.phaidra.org/kartenaale/templates/anki-pack/-/tree/main/content/Bijection-from-CSV) for cards that work both ways, where the answer is also a question,
* [content/Bijection-from-Images](https://gitlab.phaidra.org/kartenaale/templates/anki-pack/-/tree/main/content/Bijection-from-Images) for two cards, one to remember the image and one to remember the label in the filename,
* [content/Vocabulary-from-Anki](https://gitlab.phaidra.org/kartenaale/templates/anki-pack/-/tree/main/content/Vocabulary-from-Anki) for vocabulary cards, with an Anki text export of simple notes as the data source.

To get your changes back into the repository and test them, create a new branch
with your changes and open a merge request. Check the APKGs in the CI/CD build
job on the MR and if you are happy, merge to main.

### Releasing a new version
Bump the version number in `package.json` in your merge request.

When your changes land on the main branch, a new release will be created in the
releases section of the project and the kartenaale whatsapp group will be
notified (if you go to the CI/CD job and link with your Whatsapp to send out
the notification - restart the job if necessary).
